import pygame
import pygame_gui

from pygame_gui.core.ui_element import ObjectID

from client.ihm.common.component import Component


class GridPanel(Component):
    def __init__(self, pygame_manager: pygame_gui.UIManager) -> None:
        super().__init__(pygame_manager)
        self.pygame_manager = pygame_manager
        self.width = 624
        self.height = 624
        self.pos_x = 40
        self.pos_y = 40

    def init_grid_button(self) -> None:
        pos_x_button = self.pos_x
        pos_y_button = self.pos_y
        i = 0
        for i in range(13):
            for j in range(13):
                if (j % 3) == 0:
                    self.gui_element = pygame_gui.elements.UIButton(
                        relative_rect=pygame.Rect(
                            (pos_x_button, pos_y_button),
                            ((self.width / 13), (self.height / 13)),
                        ),
                        manager=self.pygame_manager,
                        text="9",
                        object_id=ObjectID(class_id="@grid_button_panel"),
                    )
                elif (j % 3) == 1:
                    self.gui_element = pygame_gui.elements.UIButton(
                        relative_rect=pygame.Rect(
                            (pos_x_button, pos_y_button),
                            ((self.width / 13), (self.height / 13)),
                        ),
                        manager=self.pygame_manager,
                        text="10",
                        object_id=ObjectID(class_id="@grid_button_panel_red_piece"),
                    )
                elif (j % 3) == 2:
                    self.gui_element = pygame_gui.elements.UIButton(
                        relative_rect=pygame.Rect(
                            (pos_x_button, pos_y_button),
                            ((self.width / 13), (self.height / 13)),
                        ),
                        manager=self.pygame_manager,
                        text="11",
                        object_id=ObjectID(class_id="@grid_button_panel_white_piece"),
                    )
                pos_x_button = pos_x_button + (self.width / 13)

            j = 0
            pos_x_button = self.pos_x
            pos_y_button = pos_y_button + (self.height / 13)

    def render(self) -> None:
        self.gui_element = pygame_gui.elements.UIPanel(
            relative_rect=pygame.Rect(
                (self.pos_x, self.pos_y), (self.width, self.height)
            ),
            manager=self.pygame_manager,
            starting_layer_height=1,
            object_id=ObjectID(class_id="@grid_panel"),
        )
        self.init_grid_button()
