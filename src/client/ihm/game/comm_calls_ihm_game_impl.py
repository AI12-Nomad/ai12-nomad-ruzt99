from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from client.ihm.game.ihm_game_controller import IHMGameController
from common.interfaces.i_comm_calls_ihm_game import I_CommCallsIHMGame
from client.ihm.game.views.game_view import GameView


class CommCallsIHMGame_Impl(I_CommCallsIHMGame):

    """

    Interface IHM Main Calls IHM Game

    """

    def __init__(self, controller: IHMGameController):
        self.controller = controller

    def notify_new_spectator(self) -> None:
        pass

    def spectator_quits_game(self) -> None:
        pass

    def update_game(self) -> None:
        pass

    def notify_new_message(self) -> None:
        pass
