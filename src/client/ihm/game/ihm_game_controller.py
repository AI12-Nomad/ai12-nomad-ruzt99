from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from client.ihm.common.py_game_controller import PyGameController

from typing import Optional

from client.ihm.game.comm_calls_ihm_game_impl import CommCallsIHMGame_Impl
from client.ihm.game.ihm_main_calls_ihm_game_impl import IHMMainCallsIHMGame_Impl
from client.ihm.game.views.game_view import GameView
from common.interfaces.i_ihm_game_calls_comm import I_IHMGameCallsComm
from common.interfaces.i_ihm_main_calls_ihm_game import I_IHMMainCallsIHMGame
from common.interfaces.i_ihm_game_calls_ihm_main import I_IHMGameCallsIHMMain
from common.interfaces.i_comm_calls_ihm_game import I_CommCallsIHMGame
from common.interfaces.i_ihm_game_calls_data import I_IHMGameCallsData


class IHMGameController:
    def __init__(
        self,
        pygame_controller: PyGameController,  # no type because of circular import
    ):
        self.pygame_controller = pygame_controller
        self.my_interface_from_ihm_main = IHMMainCallsIHMGame_Impl(self)
        self.my_interface_from_comm = CommCallsIHMGame_Impl(self)
        self.my_interface_to_comm: Optional[I_IHMGameCallsComm] = None
        self.my_interface_to_ihm_main: Optional[I_IHMGameCallsIHMMain] = None
        self.my_interface_to_data: Optional[I_IHMGameCallsData] = None

        # initialize the game view
        self.game_view = GameView(
            pygame_controller.get_ui_manager(),
            pygame_controller.get_ui_renderer(),
            self,
        )

    def set_my_interface_to_comm(self, i: I_IHMGameCallsComm) -> None:
        self.my_interface_to_comm = i

    def set_my_interface_to_ihm_main(self, i: I_IHMGameCallsIHMMain) -> None:
        self.my_interface_to_ihm_main = i

    def set_my_interface_to_data(self, i: I_IHMGameCallsData) -> None:
        self.my_interface_to_data = i

    def get_my_interface_from_ihm_main(self) -> I_IHMMainCallsIHMGame:
        if self.my_interface_from_ihm_main is not None:
            return self.my_interface_from_ihm_main
        else:
            raise Exception(
                "get_my_interface_from_ihm_main was called but my_interface_from_ihm_main is None"
            )

    def get_my_interface_from_comm(self) -> I_CommCallsIHMGame:
        if self.my_interface_from_comm is not None:
            return self.my_interface_from_comm
        else:
            raise Exception(
                "get_my_interface_from_comm was called but my_interface_from_comm is None"
            )
