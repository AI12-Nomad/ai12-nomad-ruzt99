from common.interfaces.i_ihm_game_calls_data import I_IHMGameCallsData

from typing import Dict, List, Tuple
import uuid as uuid_lib
from common import Move, LocalGame, Player, Message


class IhmGameCallsDataImpl(I_IHMGameCallsData):
    def check_move_in_local_game(self, move: Move) -> None:
        pass

    def leave_local_game(self) -> uuid_lib.UUID:
        pass

    def get_local_game(self) -> LocalGame:
        pass

    def get_local_game_grid(self) -> Dict:
        pass

    def get_local_players(self) -> Tuple[Player, Player]:
        pass

    def get_local_game_spectators(self) -> List[Player]:
        pass

    def get_local_game_messages(self) -> List[Message]:
        pass

    def is_local_game_finished(self) -> bool:
        pass

    def get_local_winner(self) -> Player:
        pass

    def clear_local_game(self) -> None:
        pass
